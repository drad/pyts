#! /usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2020 drad <drader@adercon.com>

import datetime
import logging
import os
import re
import ssl
import sys
import toml

from shutil import copyfile
from optparse import OptionParser
from urllib.request import urlopen
from urllib.error import HTTPError, URLError
from urllib.parse import parse_qs
from wsgiref.simple_server import make_server, WSGIRequestHandler
from wsgiref.util import setup_testing_defaults

available_services = [
    "ping",
    "postfile",
    "getfile",
    "pull_release",
    "get_version",
    "get_version_latest",
    "migrate_assist",
]
ping_response = "Available"  # don't change unless you know what you are doing.
about = {
    "name": "pytss",
    "version": "2.2.2",
    "modified": "2020-05-16",
    "created": "2015-06-22",
}

config = None  # config (file) data variable.

# the http request context
ctx = ssl.create_default_context()
# if you use a self signed cert on the repo you  might need to set the following
# ctx.check_hostname = False
# ctx.verify_mode = ssl.CERT_NONE

logger_base = logging.getLogger(__name__)
logger_base.setLevel(logging.INFO)
formatter = logging.Formatter("[%(levelname)s] %(message)s")
console_handler = logging.StreamHandler(stream=sys.stdout)
console_handler.setFormatter(formatter)
logger_base.addHandler(console_handler)
logger = logging.LoggerAdapter(
    logging.getLogger(__name__), {"application_name": about["name"]}
)


class NoLoggingWSGIRequestHandler(WSGIRequestHandler):
    def log_message(self, format, *args):
        if logger.isEnabledFor(logging.DEBUG):
            sys.stderr.write(
                f"{self.client_address[0]} - - [{self.log_date_time_string()}] {format%args}\n"
            )
        else:
            pass


def simple_app(environ, start_response):
    """ The web service 'app' for responding to TSS requests.
    return: http response
    """

    setup_testing_defaults(environ)  # needed for wsgi_ref server
    status = "200 OK"
    headers = [
        ("Content-type", "application/x-www-form-urlencoded"),
        # Required CORS headers.
        ("Access-Control-Allow-Origin", "*"),
        ("Access-Control-Allow-Credentials", "true"),
        (
            "Access-Control-Allow-Headers",
            "Origin, X-Requested-With, Content-Type, Accept",
        ),
    ]

    start_response(status, headers)

    queryString = parse_qs(environ["QUERY_STRING"])
    pmethod = queryString.get("method", [""])[
        0
    ]  # the method (used to ensure proper service).
    pmode = queryString.get("mode", ["rwr"])[
        0
    ]  # mode=rwr|rrw. (rwr=Read,Write,Return; rrw=Read,Return,Write).
    pfile = queryString.get("file", [""])[
        0
    ]  # file (to load for getfile; to save for postfile)

    logger.debug(f"- method={pmethod}\n- mode={pmode}\n- file={pfile}")

    if pmethod in available_services:
        if pmethod == "ping":
            r = ping()
        if pmethod == "getfile":
            r = get_file(pfile)
        if pmethod == "postfile":
            r = post_file(pfile, environ, pmode)
        if pmethod == "pull_release":
            r = pull_release()
        if pmethod == "get_version_latest":
            r = get_latest_version()
        if pmethod == "migrate_assist":
            r = migrate_assist(pfile)
        if pmethod == "get_version":
            r = get_version()

    else:
        logger.error(f"Undefined handler: {pmethod}")
        r = "Error: Undefined handler"

    yield r
    return


def get_version():
    return f"{about['name']} - v.{about['version']} ({about['modified']})"


def get_latest_version():
    """ Get the latest released version
    return: string - latest released version (tag) value
    """

    latest_version = "unavailable"
    try:
        repo = f"{config['release_repo']}"
        logger.debug(f"- getting latest from: {repo}/pytss.py")
        response = urlopen(f"{repo}/pytss.py", context=ctx)  # nosec
        latest = response.read().decode()
        #    "version": "2.0.0",
        latest_version = re.search(
            r".*version\": \"(.*)\",", latest, re.M | re.I
        ).group(1)
    except HTTPError as e:
        logger.error(f"HTTP Error: {e.code}, is releaseRepo correct/accessible?")
    except URLError as e:
        logger.error(f"URL Error: {e.reason}, is releaseRepo correct/accessible?")

    return latest_version


def check_file(the_file):
    """ Check if file exists
        note: check_file can return true even if the file does not exist for example: creating backup file.
    return: True on Success, False on error.
    """

    # skip file check for 'upgrade' requests as this is a new/special 'backup' file.
    if "pre.core.upgrade" in the_file:
        logger.info(
            "Notice: check_file skipped file check due to 'pre.core.upgrade' tag..."
        )
        return True

    if os.path.isfile(the_file):
        logger.debug("- found file...")
        return True
    else:
        logger.error(
            f"File Not Found: the file specified [{the_file}] was not found or is not accessible."
        )
        return False


def ping():
    """ Responds with a static 'response' message - used by client to see if service is available
    return: string - ping_response
    """
    return ping_response.encode()


def get_file(the_file):
    """ Get the given tw file
    return: contents of file on success, False on error
    """

    r = False
    if check_file(the_file):
        with open(the_file, "r") as f:
            r = f.read().encode()

    return r


def post_file(the_file, environ, the_mode):
    """ Accepts a http 'post' of tw file contents and writes contents to file
    return: request_body on success or False on failure
    """

    request_body_size = (
        int(environ.get("CONTENT_LENGTH")) if environ.get("CONTENT_LENGTH") else 0
    )
    request_body = (
        environ["wsgi.input"].read(request_body_size) if request_body_size > 0 else ""
    )
    if check_file(the_file):
        # write to file.
        if (
            config["write_mode"] == "safe"
            and "pre.core.upgrade" not in the_file  # noqa
        ):
            logger.debug("safe write mode specified, saving backup file...")
            file_name, file_extension = os.path.splitext(the_file)
            now = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
            backup_file = (
                f"{file_name}_{now}{file_extension}{config['backup_file_extension']}"
            )
            logger.debug(f"** backup file: {backup_file}")
            copyfile(the_file, backup_file)

        with open(the_file, "wb") as f:
            f.write(request_body)

    return str(request_body).encode() if len(request_body) > 0 else False


def load_configs(configFile):
    """ Load the configuration file
    return: sets config dictionary
    """

    global config
    try:
        config = toml.load(configFile)
    except IOError:
        logger.error(f"Configuration file error, file [{configFile}] not found.")
        sys.exit(1)


def pull_release():
    """ Downloads latest release from git repo
    return: contents of latest release
    """

    repo = f"{config['release_repo']}"
    logger.debug(f"- getting latest from: {repo}/pytss.py")
    response = urlopen(f"{repo}/pytss.py", context=ctx)  # nosec
    r = response.read()
    return r


def download_latest_release():
    """ Download the latest release
    return: no return, saves file to fs
    """

    logger.info("Downloading the latest release of Tiddlywiki-TE...")
    r = pull_release()
    logger.info("  - download complete...")
    logger.info(f"  - saving latest release as: {config['download_release_filename']}")
    with open(config["download_release_filename"], "w") as f:
        f.write(r)

    logger.info("Latest release of TiddlyWiki-TE downloaded.")
    sys.exit()


def migrate_assist(the_file):
    """ Simply picks up an alternate (migration source) tw the tiddlywiki actually handles the migration process.
    return: the file contents
    """

    r = get_file(the_file[7:])  # strip off the 'file://'.

    return r


def run_server():
    """ Run server starts the wsgi server
    """

    httpd = make_server(
        "", config["port"], simple_app, handler_class=NoLoggingWSGIRequestHandler
    )

    logger.info(get_version())
    logger.info(f'  - server port: {str(config["port"])}')
    logger.info(f'  - write mode : {str(config["write_mode"])}')
    logger.info("...server running, press ctrl+c to stop/exit")
    httpd.serve_forever()


def main():
    usage = "usage: %prog [options] arg"
    parser = OptionParser(usage)
    parser.add_option(
        "-v",
        "--verbose",
        action="store_true",
        dest="verbose",
        help="show additional runtime information",
    )
    parser.add_option(
        "--version",
        action="store_true",
        dest="show_version",
        help="show version and exit",
    )
    parser.add_option(
        "--version-latest",
        action="store_true",
        dest="check_latest_version",
        help="check the latest release version (from release repo)",
    )
    parser.add_option(
        "-c",
        "--config-file",
        dest="config_file",
        default=os.path.expanduser("~/.config/pyts/pytss.toml"),
        help="configuration file (default: ~/.config/pyts/pytss.toml)",
    )
    parser.add_option(
        "-s",
        "--serve",
        action="store_true",
        dest="run_server",
        help="start/run the wsgi server",
    )
    parser.add_option(
        "--download-release",
        action="store_true",
        dest="download_release",
        help="download the latest release of the TiddlyWiki with the TiddlySaverService bindings",
    )

    (options, args) = parser.parse_args()
    verbose = options.verbose
    if verbose:
        logger.setLevel(logging.DEBUG)

    logger.debug(f"- config file: {options.config_file}")
    load_configs(options.config_file)

    if options.show_version:
        logger.info(get_version())
        sys.exit()

    if options.download_release:
        download_latest_release()

    if options.check_latest_version:
        logger.info(f"Latest Release: {get_latest_version()}")
        sys.exit()

    if options.run_server:
        run_server()

    # call help if nothing has matched to this point.
    parser.print_help()
    sys.exit(1)


if __name__ == "__main__":
    main()
